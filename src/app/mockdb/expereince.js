export const EXPERIENCE = [{
        time: "JUL 2021 - NOW",
        text: `SA and full stack developer at Fabrinet co.
    ltd. Getting requirement from user and
    build program from requirement by use
    ASP.NET Core, HTML, JS, CSS & Oracle
    database`
    },
    {
        time: "OCT 2020 – JUN 2021",
        text: `Front end developer at buzzebees co. ltd.
    (thailand). Coding program from design by
    designer and connect API to control CRM
    privileged by use Vue.js & SCSS`
    },
    {
        time: "AUG 2018 - SEP 2020",
        text: `IT-Specialist at Celestica (Thailand) Ltd. 
    Full stack developing for control assembling
     in line and system about e-leaving by use ASP.NET core C#, 
     SQL, HTML, Angular JS , Angular, JS, CSS and jQuery`
    },
    {
        time: "JAN 2016 - JUL 2018",
        text: `Software Analysis & Full Stack developer at Siam Compressor 
    Industry Co., Ltd. in Mitsubishi Electric group. 
    My responsibility is setting full loop of SDLC in each system, 
    developing by use ASP.NET (VB or C#), SQL, 
    HTML, JAVASCRIPT, CSS and jQuery`
    }
];